# create public subnets
resource "aws_subnet" "public-sub" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-west-1a"

  tags = {
    Name = "public-sub"
  }
}

# create private subnets
resource "aws_subnet" "priv-sub" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-west-1a"

  tags = {
    Name = "private-sub"
  }
}