# create new EC2
module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  name = "techtalk-linux"

  instance_type          = "t2.micro"
  key_name               = "techtalk-key"
  subnet_id              = aws_subnet.priv-sub.id
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.allow-ssh.id]

  tags = {
    Env = "test" 
  }
}
