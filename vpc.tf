# create VPC
resource "aws_vpc" "vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "techtalk-vpc"
  }
}

# create new route for internet access
resource "aws_default_route_table" "route-table" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}
# Route table subnet association
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.public-sub.id
  route_table_id = aws_default_route_table.route-table.id
}